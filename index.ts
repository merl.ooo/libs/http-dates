/* eslint-disable @typescript-eslint/naming-convention */
const SP = '\\s';
const DIGIT = '\\d';
const day_name = '(Mon|Tue|Wed|Thu|Fri|Sat|Sun)';
const day = `(${DIGIT}{2})`;
const month = '(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)';
const year = `(${DIGIT}{4})`;
const date1 = `(${day}${SP}${month}${SP}${year})`;
const GMT = '(GMT)';
const hour = `(${DIGIT}{2})`;
const minute = `(${DIGIT}{2})`;
const second = `(${DIGIT}{2})`;
const time_of_day = `(${hour}:${minute}:${second})`;
const IMF_fixdate = new RegExp(`^${day_name},${SP}${date1}${SP}${time_of_day}${SP}${GMT}$`);

const date2 = `(${day}-${month}-(${DIGIT}{2}))`;
const day_name_l = '(Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday)';
const rfc850_date = new RegExp(`^${day_name_l},${SP}${date2}${SP}${time_of_day}${SP}${GMT}$`);

const date3 = `(${month}${SP}(${DIGIT}{2}|(${SP}${DIGIT})))`;
const asctime_date = new RegExp(`^${day_name}${SP}${date3}${SP}${time_of_day}${SP}${year}$`);
/* eslint-enable @typescript-eslint/naming-convention */

function shortenDayName(longName: string) {
  return {
    Monday: 'Mon',
    Tuesday: 'Tue',
    Wednesday: 'Wed',
    Thursday: 'Thu',
    Friday: 'Fri',
    Saturday: 'Sat',
    Sunday: 'Sun',
  }[longName];
}
function monthToInt(month: string) {
  return ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    .indexOf(month);
}
function weekdayToInt(weekday: string) {
  return ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'].indexOf(weekday);
}

export default function parseHTTPdate(input: string) {
  class HTTPdateParseError extends Error {
    constructor(message?: string) {
      super(`${message || 'Invalid RFC 7231 HTTP-date format'}: ${input}`);
    }
  }

  if (typeof input !== 'string') throw new TypeError('HTTP-date must be of type string');

  // eslint-disable-next-line max-len
  let values: { year: string | number, month: string, day: string, hours: string, minutes: string, seconds: string };
  let weekday: string;
  let rfc850: Date | undefined;

  let match: string[] | null;
  if ((match = input.match(IMF_fixdate)) !== null) { // parse IMF-fixdate format
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [all, dayName, date1, day, month, year, timeOfDay, hours, minutes, seconds] = match;
    weekday = dayName;
    values = {
      year, month, day, hours, minutes, seconds,
    };
  } else if ((match = input.match(rfc850_date)) !== null) { // parse rfc850-data format
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [all, dayNameL, date2, day, month, year, timeOfDay, hours, minutes, seconds] = match;
    weekday = shortenDayName(dayNameL) as string;
    // sent to next century if we're currently over half of century
    const now = new Date(Date.now());
    let yearRealtiveToCentury = Number.parseInt(year, 10);
    if (now.getUTCFullYear() % 100 >= 50) yearRealtiveToCentury += 100;
    const century = Math.floor(now.getUTCFullYear() / 100) * 100;
    values = {
      year: century + yearRealtiveToCentury, month, day, hours, minutes, seconds,
    };
    rfc850 = now; // mark date as rfc850
  } else if ((match = input.match(asctime_date)) !== null) { // parse asctime-date format
    // eslint-disable-next-line max-len,@typescript-eslint/no-unused-vars
    const [all, dayName, monthAndDay, month, day, day1digit, timeOfDay, hours, minutes, seconds, year] = match;
    weekday = dayName;
    values = {
      year, month, day: day1digit || day, hours, minutes, seconds,
    };
  } else {
    throw new HTTPdateParseError();
  }

  const result = new Date(0);
  result.setUTCFullYear(
    Number.parseInt(`${values.year}`, 10),
    monthToInt(values.month),
    Number.parseInt(values.day, 10),
  );
  result.setUTCHours(Number.parseInt(values.hours, 10));
  result.setUTCMinutes(Number.parseInt(values.minutes, 10));
  result.setUTCSeconds(Number.parseInt(values.seconds, 10));
  // substract 100 years if RFC 850 date appears to be more than 50 years in the future
  if (rfc850) {
    const now = rfc850;
    now.setUTCFullYear(rfc850.getUTCFullYear() + 50);
    if (result > now) result.setUTCFullYear(result.getUTCFullYear() - 100);
  }
  if (result.getUTCDay() !== weekdayToInt(weekday)) {
    // eslint-disable-next-line max-len
    throw new HTTPdateParseError(`Invalid HTTP-date format: day-name ${weekday} does not match date`);
  }
  return result;
}
