/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
// eslint-disable-next-line unicorn/prefer-module
module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  coverageReporters: ['text', 'json-summary'],
};
