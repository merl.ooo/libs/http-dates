[![npm version](https://badge.fury.io/js/http-dates.svg)](https://badge.fury.io/js/http-dates)
![package size](https://img.shields.io/bundlephobia/min/http-dates)
![test coverage](./coverage/badge.svg)

# http-dates

Ultra slim, zero-dependency library for parsing HTTP-dates according to
[RFC 7231, Section 7.1.1.1](https://httpwg.org/specs/rfc7231.html#http.date).
E.g. as being used in `Last-Modified` and `If-Unmodified-Since` headers.
All the described formats are being parsed for full backwards compatibility
- [`IMF-fixdate`](https://httpwg.org/specs/rfc7231.html#preferred.date.format)
- [`obs-date`](https://httpwg.org/specs/rfc7231.html#obsolete.date.formats)
  - `rfc850-date`
  - `asctime-date`

## Installation

```shell
npm i http-dates
```

## Usage

Just parse your HTTP header content (or wherever you get the HTTP-date formatted string from) like:
```js
import parseHTTPdate from 'http-dates';

// IMF-fixdate for 1994-11-06T08:49:37.000Z
parseHTTPdate('Sun, 06 Nov 1994 08:49:37 GMT');

// rfc850-date for 1994-11-06T08:49:37.000Z
// NOTE: next line will not work from November 7th 2044 (see deprecation notes below)
parseHTTPdate('Sunday, 06-Nov-94 08:49:37 GMT');

// asctime-date 1994-11-06T08:49:37.000Z
parseHTTPdate('Sun Nov  6 08:49:37 1994');
```

## Deprecation notes

### asctime-date

ANSI C's `asctime()` format is deprecated for legacy reasons.

### rfc850-date

The use of RFC 850 date format is highly discouraged as it is a relative date format.
RFC 7231 states:
> Recipients of a timestamp value in rfc850-date format, which uses a two-digit year, MUST interpret a timestamp that appears to be more than 50 years in the future as representing the most recent year in the past that had the same last two digits.

Thsi is being handled by the library, leading to indeterministic parsing results:
If your parse a RFC 850 date string `'Tuesday, 06-Nov-73 08:49:37 GMT'` today (March 2022)
it returns a Date in 1973 (`1973-11-06T08:49:37.000Z`).
If you repeat the same call in 2024 it would parse it as a date in 2073 (`2073-11-06T08:49:37.000Z`).
but it will NOT, because that day is a not Tuesday, so it will throw a `HTTPdateParseError`.
